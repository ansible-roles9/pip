# [0.8.0](/Ansible_Roles/pip/compare/0.8.0...main)
* CICD migraton from Jenkins to GitlabCI
* Update exceptions for *test* in order to work with *Gitlab*

# [0.7.0](/Ansible_Roles/pip/compare/0.7.0...main)
* Se configuran las versiones del `CHANGELOG.md` para que sean revisables por la herramienta de comparación de **Gitea**
* Se actualiza *meta* indicando la versión de `ansible` máxima soportada.

# [0.6.0](/Ansible_Roles/pip/compare/0.6.0...main)
* Se agrega el paquete *python3-dev*, necesario cuando se compilan paquetes con pip
* Se agrega una tarea para que siempre actualice **pip**
* Se ajusta el `Jenkinsfile` con nueva versión de taggeado de versiones

# [0.5.0](/Ansible_Roles/pip/compare/0.5.0...main)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# [0.4.0](/Ansible_Roles/pip/compare/0.4.0...main)
* Se ajusta el `.editorconfig`, `.gitignore` con las nuevas reglas bajo el estándar de plantilla

# [0.3.0](/Ansible_Roles/pip/compare/0.3.0...main)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `ansiblePipeline`
* Se ajusta el `README` para que muestre información relevante
* Se agrega el acuerdo de licencia para el proyecto

# [0.2.0](/Ansible_Roles/pip/compare/0.2.0...main)
* Se ajusta la tarea de instalación de paquetes al `venv`, en ves de recorrer el diccionario con el método `with_items` y llamar el valor directamente, usamos el metodo `with_dict`, ya que el el valor a leer es un diccionario y no una lista.

# [0.1.1](/Ansible_Roles/pip/compare/0.1.1...main)
* Actualizamos `Jenkinsfile` para adaptarnos a la nueva versión del vars `ansibleActions`[jenkins-shared-libraries]

# [0.1.0](/Ansible_Roles/pip/compare/0.1.0...main)
* Se inicializa el *CHANGELOG* para un mejor seguimiento de los cambios
* Se ajusta el nombre de una de las tareas del rol y se comenta una tarea que no tenía nombre
* Se activa el **update_cache** para cada vez que se haga la instalación de requerimientos
* Se actualiza el *Jenkinsfile* con la configuración adaptada para las nuevas librerías
