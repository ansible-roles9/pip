[![pipeline status](https://gitlab.com/ansible-roles9/pip/badges/development/pipeline.svg)](https://gitlab.com/ansible-roles9/pip/-/commits/development) ![Project version](https://img.shields.io/gitlab/v/tag/ansible-roles9/pip)![Project license](https://img.shields.io/gitlab/license/ansible-roles9/pip)

Role PIP
===================

Description
-------------
The purpose of this role is configure python jails over **python 3** and **pip** packages.

Requirements
-------------
It has no special requirements

How to use
-------------
    - import_role:
        name: "pip"
      vars:
        environments:
            [jail_name]:
                - [package_name]
                - [package_name]
                - [package_name==0.1]
            [jail_name]:
                - [package_name]
                - [package_name]
                - [package_name==2]
            [jail_name]: []
ROOT Vars
-------------

* Variable name: `environments`
* Default value: empty {}.
* Accepted values: Dictionary [key => value].
* Description: Variable intended to contain `keys` for the purposes of each `python virtualenv` and `values` as package list.

CHILD Vars for: `environments`
-------------

* Variable name: `jail_name`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The python jail name that you want to create.

----------

* Variable name: `package_name`
* Default value: `NULL`.
* Accepted values: STRING
* Description: The package name that you want to install, this content is a list so if you want to declare as empty can use `[]`. You can specify the package version like this: `package==version`, by example **yamllint==0.1**

Legend
-------------
* NKS => No key sensitive
* KS => Key sensitive
